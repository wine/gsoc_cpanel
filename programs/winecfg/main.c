/*
 * WineCfg main entry point
 *
 * Copyright 2002 Jaco Greeff
 * Copyright 2003 Dimitrie O. Paun
 * Copyright 2003 Mike Hearn
 * Copyright 2008 Owen Rudge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#define WIN32_LEAN_AND_MEAN

#define NONAMELESSUNION
#define NONAMELESSSTRUCT

#include <windows.h>
#include <commctrl.h>
#include <objbase.h>
#include <wine/debug.h>

#include "resource.h"
#include "winecfg.h"

WINE_DEFAULT_DEBUG_CHANNEL(winecfg);

static INT CALLBACK
PropSheetCallback (HWND hWnd, UINT uMsg, LPARAM lParam)
{
    switch (uMsg)
    {
	/*
	 * hWnd = NULL, lParam == dialog resource
	 */
    case PSCB_PRECREATE:
	break;

    case PSCB_INITIALIZED:
	break;

    default:
	break;
    }
    return 0;
}

static INT_PTR CALLBACK
AboutDlgProc (HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    char *owner, *org;

    switch (uMsg) {

	case WM_NOTIFY:
	    switch(((LPNMHDR)lParam)->code)
	    {
            case PSN_APPLY:
                /*save registration info to registry */
                owner = get_text(hDlg, IDC_ABT_OWNER);
                org   = get_text(hDlg, IDC_ABT_ORG);

                set_reg_key(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion",
                            "RegisteredOwner", owner ? owner : "");
                set_reg_key(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion",
                            "RegisteredOrganization", org ? org : "");
                set_reg_key(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows NT\\CurrentVersion",
                            "RegisteredOwner", owner ? owner : "");
                set_reg_key(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows NT\\CurrentVersion",
                            "RegisteredOrganization", org ? org : "");
                apply();

                HeapFree(GetProcessHeap(), 0, owner);
                HeapFree(GetProcessHeap(), 0, org);
                break;
            }
            break;

        case WM_INITDIALOG:
            /* read owner and organization info from registry, load it into text box */
            owner = get_reg_key(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows NT\\CurrentVersion",
                                "RegisteredOwner", "");
            org =   get_reg_key(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows NT\\CurrentVersion",
                                "RegisteredOrganization", "");

            SetDlgItemText(hDlg, IDC_ABT_OWNER, owner);
            SetDlgItemText(hDlg, IDC_ABT_ORG, org);

            SendMessage(GetParent(hDlg), PSM_UNCHANGED, 0, 0);

            HeapFree(GetProcessHeap(), 0, owner);
            HeapFree(GetProcessHeap(), 0, org);
            break;

	case WM_COMMAND:
            switch(HIWORD(wParam))
            {
            case EN_CHANGE:
		/* enable apply button */
                SendMessage(GetParent(hDlg), PSM_CHANGED, 0, 0);
                break;
            }
            break;
    }
    return FALSE;
}

#define NUM_PROPERTY_PAGES 7

#define TAB_APPLICATIONS   1
#define TAB_LIBRARIES      2
#define TAB_GRAPHICS       4
#define TAB_DESKTOP        8
#define TAB_DRIVES        16
#define TAB_AUDIO         32
#define TAB_ABOUT         64

#define TAB_TITLE_DESKTOP  1
#define TAB_TITLE_DESKTOP  1
#define TAB_TITLE_SOUND    2

#define TAB_DEFAULT	(TAB_APPLICATIONS | TAB_LIBRARIES | TAB_GRAPHICS | \
                         TAB_DRIVES | TAB_AUDIO | TAB_ABOUT)

static int iTabMask = 0;
int iWindowCaption = IDS_WINECFG_TITLE;

static INT_PTR
doPropertySheet (HINSTANCE hInstance, HWND hOwner)
{
    PROPSHEETPAGEW psp[NUM_PROPERTY_PAGES];
    PROPSHEETHEADERW psh;
    int pg = 0; /* start with page 0 */

    /*
     * Fill out the (Applications) PROPSHEETPAGE data structure 
     * for the property sheet
     */
    if (iTabMask & TAB_APPLICATIONS)
    {
        psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
        psp[pg].dwFlags = PSP_USETITLE;
        psp[pg].hInstance = hInstance;
        psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_APPCFG);
        psp[pg].u2.pszIcon = NULL;
        psp[pg].pfnDlgProc = AppDlgProc;
        psp[pg].pszTitle = load_string (IDS_TAB_APPLICATIONS);
        psp[pg].lParam = 0;
        pg++;
    }

    /*
     * Fill out the (Libraries) PROPSHEETPAGE data structure 
     * for the property sheet
     */
    if (iTabMask & TAB_LIBRARIES)
    {
       psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
       psp[pg].dwFlags = PSP_USETITLE;
       psp[pg].hInstance = hInstance;
       psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_DLLCFG);
       psp[pg].u2.pszIcon = NULL;
       psp[pg].pfnDlgProc = LibrariesDlgProc;
       psp[pg].pszTitle = load_string (IDS_TAB_DLLS);
       psp[pg].lParam = 0;
       pg++;
    }
 
    /*
     * Fill out the (X11Drv) PROPSHEETPAGE data structure 
     * for the property sheet
     */
    if (iTabMask & TAB_GRAPHICS)
    {
       psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
       psp[pg].dwFlags = PSP_USETITLE;
       psp[pg].hInstance = hInstance;
       psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_GRAPHCFG);
       psp[pg].u2.pszIcon = NULL;
       psp[pg].pfnDlgProc = GraphDlgProc;
       psp[pg].pszTitle =  load_string (IDS_TAB_GRAPHICS);
       psp[pg].lParam = 0;
       pg++;
    }

    if (iTabMask & TAB_DESKTOP)
    {
       psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
       psp[pg].dwFlags = PSP_USETITLE;
       psp[pg].hInstance = hInstance;
       psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_DESKTOP_INTEGRATION);
       psp[pg].u2.pszIcon = NULL;
       psp[pg].pfnDlgProc = ThemeDlgProc;
       psp[pg].pszTitle =  load_string (IDS_TAB_DESKTOP_INTEGRATION);
       psp[pg].lParam = 0;
       pg++;
    }

    if (iTabMask & TAB_DRIVES)
    {
       psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
       psp[pg].dwFlags = PSP_USETITLE;
       psp[pg].hInstance = hInstance;
       psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_DRIVECFG);
       psp[pg].u2.pszIcon = NULL;
       psp[pg].pfnDlgProc = DriveDlgProc;
       psp[pg].pszTitle =  load_string (IDS_TAB_DRIVES);
       psp[pg].lParam = 0;
       pg++;
    }

    if (iTabMask & TAB_AUDIO)
    {
       psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
       psp[pg].dwFlags = PSP_USETITLE;
       psp[pg].hInstance = hInstance;
       psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_AUDIOCFG);
       psp[pg].u2.pszIcon = NULL;
       psp[pg].pfnDlgProc = AudioDlgProc;
       psp[pg].pszTitle =  load_string (IDS_TAB_AUDIO);
       psp[pg].lParam = 0;
       pg++;
    }

    /*
     * Fill out the (General) PROPSHEETPAGE data structure 
     * for the property sheet
     */
    if (iTabMask & TAB_ABOUT)
    {
       psp[pg].dwSize = sizeof (PROPSHEETPAGEW);
       psp[pg].dwFlags = PSP_USETITLE;
       psp[pg].hInstance = hInstance;
       psp[pg].u.pszTemplate = MAKEINTRESOURCEW (IDD_ABOUTCFG);
       psp[pg].u2.pszIcon = NULL;
       psp[pg].pfnDlgProc = AboutDlgProc;
       psp[pg].pszTitle =  load_string (IDS_TAB_ABOUT);
       psp[pg].lParam = 0;
       pg++;
    }

    /*
     * Fill out the PROPSHEETHEADER
     */
    psh.dwSize = sizeof (PROPSHEETHEADERW);
    psh.dwFlags = PSH_PROPSHEETPAGE | PSH_USEICONID | PSH_USECALLBACK;
    psh.hwndParent = hOwner;
    psh.hInstance = hInstance;
    psh.u.pszIcon = NULL;
    psh.pszCaption =  load_string (iWindowCaption);
    psh.nPages = pg;
    psh.u3.ppsp = psp;
    psh.pfnCallback = PropSheetCallback;
    psh.u2.nStartPage = 0;

    /*
     * Display the modal property sheet
     */
    return PropertySheetW (&psh);
}

static BOOL handle_arg_autodetect(int data)
{
    gui_mode = FALSE;

    if (autodetect_drives()) {
        apply_drive_changes();
    }

    return(TRUE);
}

static BOOL handle_arg_tab(int data)
{
    iTabMask |= data;
    return(FALSE);
}

static BOOL handle_arg_title(int data)
{
    iWindowCaption = data;
    return(FALSE);
}

#define MAX_ARGS          20

typedef struct {
    char *name;               /* name of argument */
    BOOL (*proc)(int data);   /* procedure to handle argument */
    int data;                 /* data to pass to proc */
} CMDLINE_ARGS;

CMDLINE_ARGS command_args[] = {
    {"/d", handle_arg_autodetect, 0},
    {"-d", handle_arg_autodetect, 0},
    {"apps", handle_arg_tab, TAB_APPLICATIONS},
    {"libs", handle_arg_tab, TAB_LIBRARIES},
    {"gfx", handle_arg_tab, TAB_GRAPHICS},
    {"desk", handle_arg_tab, TAB_DESKTOP},
    {"drives", handle_arg_tab, TAB_DRIVES},
    {"audio", handle_arg_tab, TAB_AUDIO},
    {"about", handle_arg_tab, TAB_ABOUT},
    {"/title:desktop", handle_arg_title, IDS_TITLE_DESKTOP},
    {"/title:sound", handle_arg_title, IDS_TITLE_SOUND},
    {NULL, NULL, 0}
};

/******************************************************************************
 * Name       : ParseCmdLine
 * Description: Converts a string to an argv-style array of arguments
 * Parameters : in       - the command line
 *              max_argc - the maximum number of arguments to parse
 *              argv     - pointer to an array of strings for output
 * Returns    : The number of arguments found (argc)
 */
static int
ParseCommandLine(char *in, int max_argc, char **argv)
{
    int n = 0;

    do {
        /* skip any whitespace at the start */
        while (*in == ' ' || *in == '\t')
            in++;

        /* reached the end? */
        if (*in == 0)
            break;
        
        /* if we find quotes, parse as one argument */
        if (*in == '"') {
            argv[n++] = ++in;

            while (*in != '"') {
                if (*in == 0)
                    return n;

                in++;
            }
        } else {
            argv[n++] = in;

            /* skip any more whitespace */
            while (*in != ' ' && *in != '\t') {
                if (*in == 0)
                    return(n);

                in++;
            }
        }

        *in++ = 0;
    } while (n != max_argc);

    return(n);
}

/******************************************************************************
 * Name       : ProcessCmdLine
 * Description: Checks command line parameters for any valid options
 * Parameters : lpCmdLine - the command line
 * Returns    : TRUE - if a command line parameter signalled we should exit
 *              FALSE - if no parameter signalled an exit
 */
static BOOL
ProcessCmdLine(LPSTR lpCmdLine)
{
    char *argv[MAX_ARGS];
    int argc = 0;
    CMDLINE_ARGS *args, *args_ptr;
    int i;
    BOOL ret = FALSE;

    /* convert command line to argc/argv format */
    argc = ParseCommandLine(lpCmdLine, MAX_ARGS, argv);

    if (argc == 0)
        return FALSE;

    args = args_ptr = command_args;
    
    /* loop through arguments we have to parse */
    while (args_ptr) {
        if (args_ptr->name == NULL)
            break;

        for (i = 0; i < argc; i++) {
            if (lstrcmpi(argv[i], args_ptr->name) == 0) {
                /* argument found, so call its handling procedure */
                if (args_ptr->proc != NULL) {
                    if (args_ptr->proc(args_ptr->data))
                        ret = TRUE;
                }
            }
        }

        args_ptr++;
    }

    return(ret);
}

/*****************************************************************************
 * Name       : WinMain
 * Description: Main windows entry point
 * Parameters : hInstance
 *              hPrev
 *              szCmdLine
 *              nShow
 * Returns    : Program exit code
 */
int WINAPI
WinMain (HINSTANCE hInstance, HINSTANCE hPrev, LPSTR szCmdLine, int nShow)
{
    if (ProcessCmdLine(szCmdLine)) {
        return 0;
    }

    if (initialize(hInstance) != 0) {
	WINE_ERR("initialization failed, aborting\n");
	ExitProcess(1);
    }
 
    /*
     * The next 9 lines should be all that is needed
     * for the Wine Configuration property sheet
     */
    InitCommonControls ();
    CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

    if (iTabMask == 0)
        iTabMask = TAB_DEFAULT;

    if (doPropertySheet (hInstance, NULL) > 0) {
	WINE_TRACE("OK\n");
    } else {
	WINE_TRACE("Cancel\n");
    }
    CoUninitialize(); 
    ExitProcess (0);

    return 0;
}
