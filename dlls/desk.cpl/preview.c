/*
 * Desktop control panel
 * Preview window
 *
 * Copyright (c) 2006, 2007 Eric Kohl
 * Copyright (c) 2008 Owen Rudge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#include "config.h"
#include "wine/port.h"
#include "wine/unicode.h"
#include "wine/debug.h"

#define COBJMACROS

#include <windef.h>
#include <winbase.h>
#include <wingdi.h>
#include <winuser.h>

#include "resource.h"
#include "desk.h"
#include "preview.h"

WINE_DEFAULT_DEBUG_CHANNEL(deskcpl);

/* user32 internal function */
DWORD WINAPI DrawMenuBarTemp(HWND hwnd, HDC hDC, LPRECT lprect, HMENU hMenu, HFONT hFont);

static const WCHAR szPreviewWndClass[] = {'D','e','s','k','P','r','e','v','C','l','s',0};

typedef struct _PREVIEW_DATA
{
    HWND hwndParent;

    DWORD clrSysColor[COLOR_MENUBAR];

    HBRUSH hbrScrollbar;
    HBRUSH hbrDesktop;
    HBRUSH hbrWindow;

    INT cxEdge;
    INT cyEdge;

    INT cySizeFrame;

    INT cyCaption;
    INT cyBorder;
    INT cyMenu;
    INT cxScrollbar;

    RECT rcDesktop;
    RECT rcInactiveFrame;
    RECT rcInactiveCaption;
    RECT rcInactiveCaptionButtons;

    RECT rcActiveFrame;
    RECT rcActiveCaption;
    RECT rcActiveCaptionButtons;
    RECT rcActiveMenuBar;
    RECT rcSelectedMenuItem;
    RECT rcActiveClient;
    RECT rcActiveScroll;

    RECT rcDialogFrame;
    RECT rcDialogCaption;
    RECT rcDialogCaptionButtons;
    RECT rcDialogClient;

    RECT rcDialogButton;

    LPWSTR lpInAct;
    LPWSTR lpAct;
    LPWSTR lpWinTxt;
    LPWSTR lpMessBox;
    LPWSTR lpMessText;
    LPWSTR lpButText;

    LOGFONTW lfCaptionFont;
    LOGFONTW lfMenuFont;
    LOGFONTW lfMessageFont;

    HFONT hCaptionFont;
    HFONT hMenuFont;
    HFONT hMessageFont;

    HMENU hMenu;

} PREVIEW_DATA, *PPREVIEW_DATA;

static LPWSTR AllocAndLoadString(HINSTANCE hInst, int id)
{
    LPWSTR buf = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_STRING_LEN *
        sizeof(WCHAR));
    LoadStringW(hInst, id, buf, MAX_STRING_LEN);

    return(buf);
}

static VOID DrawCaptionButtons(HDC hdc, LPRECT lpRect, BOOL bMinMax, int x)
{
    RECT rc3;
    RECT rc4;
    RECT rc5;

    rc3.left = lpRect->right - 2 - x;
    rc3.top = lpRect->top + 2;
    rc3.right = lpRect->right - 2;
    rc3.bottom = lpRect->bottom - 2;

    DrawFrameControl(hdc, &rc3, DFC_CAPTION, DFCS_CAPTIONCLOSE);

    if (bMinMax)
    {
        rc4.left = rc3.left - x - 2;
        rc4.top = rc3.top;
        rc4.right = rc3.right - x - 2;
        rc4.bottom = rc3.bottom;

        DrawFrameControl(hdc, &rc4, DFC_CAPTION, DFCS_CAPTIONMAX);

        rc5.left = rc4.left - x;
        rc5.top = rc4.top;
        rc5.right = rc4.right - x;
        rc5.bottom = rc4.bottom;

        DrawFrameControl(hdc, &rc5, DFC_CAPTION, DFCS_CAPTIONMIN);
    }
}

static VOID DrawScrollbar(HDC hdc, LPRECT rc, HBRUSH hbrScrollbar)
{
    RECT rcTop;
    RECT rcBottom;
    RECT rcMiddle;
    int width;

    width = rc->right - rc->left;

    rcTop.left = rc->left;
    rcTop.right = rc->right;
    rcTop.top = rc->top;
    rcTop.bottom = rc->top + width;

    rcMiddle.left = rc->left;
    rcMiddle.right = rc->right;
    rcMiddle.top = rc->top + width;
    rcMiddle.bottom = rc->bottom - width;

    rcBottom.left = rc->left;
    rcBottom.right = rc->right;
    rcBottom.top = rc->bottom - width;
    rcBottom.bottom = rc->bottom;

    DrawFrameControl(hdc, &rcTop, DFC_SCROLL, DFCS_SCROLLUP);
    DrawFrameControl(hdc, &rcBottom, DFC_SCROLL, DFCS_SCROLLDOWN);

    FillRect(hdc, &rcMiddle, hbrScrollbar);
}

static VOID OnCreate(HWND hwnd, PPREVIEW_DATA pPreviewData)
{
    NONCLIENTMETRICSW NonClientMetrics;
    INT i;

    for (i = 0; i < COLOR_MENUBAR + 1; i++)
    {
        pPreviewData->clrSysColor[i] = GetSysColor(i);
    }

    pPreviewData->hbrScrollbar = CreateSolidBrush(pPreviewData->clrSysColor[COLOR_SCROLLBAR]);
    pPreviewData->hbrDesktop = CreateSolidBrush(pPreviewData->clrSysColor[COLOR_DESKTOP]);
    pPreviewData->hbrWindow = CreateSolidBrush(pPreviewData->clrSysColor[COLOR_WINDOW]);

    pPreviewData->cxEdge = GetSystemMetrics(SM_CXEDGE) - 2;
    pPreviewData->cyEdge = GetSystemMetrics(SM_CXEDGE) - 2;

    pPreviewData->cySizeFrame = GetSystemMetrics(SM_CYSIZEFRAME) - 1;

    pPreviewData->cyCaption = GetSystemMetrics(SM_CYCAPTION);
    pPreviewData->cyMenu = GetSystemMetrics(SM_CYMENU);
    pPreviewData->cxScrollbar = GetSystemMetrics(SM_CXVSCROLL);
    pPreviewData->cyBorder = GetSystemMetrics(SM_CYBORDER);

    /* load font info */
    NonClientMetrics.cbSize = sizeof(NONCLIENTMETRICSW);
    SystemParametersInfoW(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICSW),
        &NonClientMetrics, 0);

    pPreviewData->lfCaptionFont = NonClientMetrics.lfCaptionFont;
    pPreviewData->hCaptionFont = CreateFontIndirectW(&pPreviewData->lfCaptionFont);

    pPreviewData->lfMenuFont = NonClientMetrics.lfMenuFont;
    pPreviewData->hMenuFont = CreateFontIndirectW(&pPreviewData->lfMenuFont);

    pPreviewData->lfMessageFont = NonClientMetrics.lfMessageFont;
    pPreviewData->hMessageFont = CreateFontIndirectW(&pPreviewData->lfMessageFont);

    /* Load and modify the menu */
    pPreviewData->hMenu = LoadMenuW(hInstCPL, MAKEINTRESOURCEW(IDR_PREVIEW_MENU));
    EnableMenuItem(pPreviewData->hMenu, 1, MF_BYPOSITION | MF_GRAYED);
    HiliteMenuItem(hwnd, pPreviewData->hMenu, 2, MF_BYPOSITION | MF_HILITE);

    /* Load misc strings */
    pPreviewData->lpInAct = AllocAndLoadString(hInstCPL, IDS_INACTWIN);
    pPreviewData->lpAct = AllocAndLoadString(hInstCPL, IDS_ACTWIN);
    pPreviewData->lpWinTxt = AllocAndLoadString(hInstCPL, IDS_WINTEXT);
    pPreviewData->lpMessBox = AllocAndLoadString(hInstCPL, IDS_MESSBOX);
    pPreviewData->lpMessText = AllocAndLoadString(hInstCPL, IDS_MESSTEXT);
    pPreviewData->lpButText = AllocAndLoadString(hInstCPL, IDS_BUTTEXT);
}

static VOID CalculateItemSize(PPREVIEW_DATA pPreviewData)
{
    int width, height;

    /* Calculate the inactive window rectangle */
    pPreviewData->rcInactiveFrame.left = pPreviewData->rcDesktop.left + 8;
    pPreviewData->rcInactiveFrame.top = pPreviewData->rcDesktop.top + 8;
    pPreviewData->rcInactiveFrame.right = pPreviewData->rcDesktop.right - 25;
    pPreviewData->rcInactiveFrame.bottom = pPreviewData->rcDesktop.bottom - 30;

    /* Calculate the inactive caption rectangle */
    pPreviewData->rcInactiveCaption.left = pPreviewData->rcInactiveFrame.left +
        pPreviewData->cxEdge + pPreviewData->cySizeFrame + 1;
    pPreviewData->rcInactiveCaption.top = pPreviewData->rcInactiveFrame.top +
        pPreviewData->cyEdge + pPreviewData->cySizeFrame + 1;
    pPreviewData->rcInactiveCaption.right = pPreviewData->rcInactiveFrame.right
        - pPreviewData->cxEdge - pPreviewData->cySizeFrame - 1;
    pPreviewData->rcInactiveCaption.bottom = pPreviewData->rcInactiveCaption.top
        + pPreviewData->cyCaption - pPreviewData->cyBorder;

    /* Calculate the inactive caption buttons rectangle */
    pPreviewData->rcInactiveCaptionButtons.left = pPreviewData->rcInactiveCaption.right -
        2 - 2 - 3 * 16;
    pPreviewData->rcInactiveCaptionButtons.top = pPreviewData->rcInactiveCaption.top + 2;
    pPreviewData->rcInactiveCaptionButtons.right = pPreviewData->rcInactiveCaption.right - 2;
    pPreviewData->rcInactiveCaptionButtons.bottom = pPreviewData->rcInactiveCaption.bottom - 2;

    /* Calculate the active window rectangle */
    pPreviewData->rcActiveFrame.left = pPreviewData->rcInactiveFrame.left + 3 +
        pPreviewData->cySizeFrame;
    pPreviewData->rcActiveFrame.top = pPreviewData->rcInactiveCaption.bottom + 1;
    pPreviewData->rcActiveFrame.right = pPreviewData->rcDesktop.right - 10;
    pPreviewData->rcActiveFrame.bottom = pPreviewData->rcDesktop.bottom - 25;

    /* Calculate the active caption rectangle */
    pPreviewData->rcActiveCaption.left = pPreviewData->rcActiveFrame.left +
        pPreviewData->cxEdge + pPreviewData->cySizeFrame + 1;
    pPreviewData->rcActiveCaption.top = pPreviewData->rcActiveFrame.top +
        pPreviewData->cxEdge + pPreviewData->cySizeFrame + 1;
    pPreviewData->rcActiveCaption.right = pPreviewData->rcActiveFrame.right -
        pPreviewData->cxEdge - pPreviewData->cySizeFrame - 1;
    pPreviewData->rcActiveCaption.bottom = pPreviewData->rcActiveCaption.top +
        pPreviewData->cyCaption - pPreviewData->cyBorder;

    /* Calculate the active caption buttons rectangle */
    pPreviewData->rcActiveCaptionButtons.left = pPreviewData->rcActiveCaption.right -
        2 - 2 - 3 * 16;
    pPreviewData->rcActiveCaptionButtons.top = pPreviewData->rcActiveCaption.top + 2;
    pPreviewData->rcActiveCaptionButtons.right = pPreviewData->rcActiveCaption.right - 2;
    pPreviewData->rcActiveCaptionButtons.bottom = pPreviewData->rcActiveCaption.bottom - 2;

    /* Calculate the active menu bar rectangle */
    pPreviewData->rcActiveMenuBar.left = pPreviewData->rcActiveFrame.left +
        pPreviewData->cxEdge + pPreviewData->cySizeFrame + 1;
    pPreviewData->rcActiveMenuBar.top = pPreviewData->rcActiveCaption.bottom + 1;
    pPreviewData->rcActiveMenuBar.right = pPreviewData->rcActiveFrame.right -
        pPreviewData->cxEdge - pPreviewData->cySizeFrame - 1;
    pPreviewData->rcActiveMenuBar.bottom = pPreviewData->rcActiveMenuBar.top +
        pPreviewData->cyMenu + 1;

    /* Calculate the active client rectangle */
    pPreviewData->rcActiveClient.left = pPreviewData->rcActiveFrame.left +
        pPreviewData->cxEdge + pPreviewData->cySizeFrame + 1;
    pPreviewData->rcActiveClient.top = pPreviewData->rcActiveMenuBar.bottom;
    pPreviewData->rcActiveClient.right = pPreviewData->rcActiveFrame.right -
        pPreviewData->cxEdge - pPreviewData->cySizeFrame - 1;
    pPreviewData->rcActiveClient.bottom = pPreviewData->rcActiveFrame.bottom -
        pPreviewData->cyEdge - pPreviewData->cySizeFrame - 1;

    /* Calculate the active scroll rectangle */
    pPreviewData->rcActiveScroll.left = pPreviewData->rcActiveClient.right - 2 -
        pPreviewData->cxScrollbar;
    pPreviewData->rcActiveScroll.top = pPreviewData->rcActiveClient.top + 2;
    pPreviewData->rcActiveScroll.right = pPreviewData->rcActiveClient.right - 2;
    pPreviewData->rcActiveScroll.bottom = pPreviewData->rcActiveClient.bottom - 2;


    /* Dialog window */
    pPreviewData->rcDialogFrame.left = pPreviewData->rcActiveClient.left + 4;
    pPreviewData->rcDialogFrame.top = (pPreviewData->rcDesktop.bottom * 60) / 100;
    pPreviewData->rcDialogFrame.right = (pPreviewData->rcDesktop.right * 65) / 100;
    pPreviewData->rcDialogFrame.bottom = pPreviewData->rcDesktop.bottom - 5;

    /* Calculate the dialog caption rectangle */
    pPreviewData->rcDialogCaption.left = pPreviewData->rcDialogFrame.left + 3;
    pPreviewData->rcDialogCaption.top = pPreviewData->rcDialogFrame.top + 3;
    pPreviewData->rcDialogCaption.right = pPreviewData->rcDialogFrame.right - 3;
    pPreviewData->rcDialogCaption.bottom = pPreviewData->rcDialogFrame.top +
        pPreviewData->cyCaption + 1 + 1;

    /* Calculate the inactive caption buttons rectangle */
    pPreviewData->rcDialogCaptionButtons.left = pPreviewData->rcDialogCaption.right -
        2 - 16;
    pPreviewData->rcDialogCaptionButtons.top = pPreviewData->rcDialogCaption.top + 2;
    pPreviewData->rcDialogCaptionButtons.right = pPreviewData->rcDialogCaption.right - 2;
    pPreviewData->rcDialogCaptionButtons.bottom = pPreviewData->rcDialogCaption.bottom - 2;

    /* Calculate the dialog client rectangle */
    pPreviewData->rcDialogClient.left = pPreviewData->rcDialogFrame.left + 3;
    pPreviewData->rcDialogClient.top = pPreviewData->rcDialogCaption.bottom + 1;
    pPreviewData->rcDialogClient.right = pPreviewData->rcDialogFrame.right - 3;
    pPreviewData->rcDialogClient.bottom = pPreviewData->rcDialogFrame.bottom - 3;

    /* Calculate the dialog button rectangle */
    width = 80;
    height = 28;

    pPreviewData->rcDialogButton.left =
        (pPreviewData->rcDialogClient.right + pPreviewData->rcDialogClient.left - width) / 2;
    pPreviewData->rcDialogButton.right = pPreviewData->rcDialogButton.left + width;
    pPreviewData->rcDialogButton.bottom = pPreviewData->rcDialogClient.bottom - 2;
    pPreviewData->rcDialogButton.top = pPreviewData->rcDialogButton.bottom - height;
}


static VOID OnSize(INT cx, INT cy, PPREVIEW_DATA pPreviewData)
{
    /* Get Desktop rectangle */
    pPreviewData->rcDesktop.left = 0;
    pPreviewData->rcDesktop.top = 0;
    pPreviewData->rcDesktop.right = cx;
    pPreviewData->rcDesktop.bottom = cy;

    CalculateItemSize(pPreviewData);
}

static VOID OnPaint(HWND hwnd, PPREVIEW_DATA pPreviewData)
{
    PAINTSTRUCT ps;
    HFONT hOldFont;
    HDC hdc;
    RECT rc;

    hdc = BeginPaint(hwnd, &ps);

    /* Desktop */
    FillRect(hdc, &pPreviewData->rcDesktop, pPreviewData->hbrDesktop);

    /* Inactive Window */
    DrawEdge(hdc, &pPreviewData->rcInactiveFrame, EDGE_RAISED, BF_RECT | BF_MIDDLE);
    SetTextColor(hdc, pPreviewData->clrSysColor[COLOR_INACTIVECAPTIONTEXT]);
    DrawCaptionTempW(NULL, hdc, &pPreviewData->rcInactiveCaption,  pPreviewData->hCaptionFont,
                    NULL, pPreviewData->lpInAct, DC_GRADIENT | DC_ICON | DC_TEXT);
    DrawCaptionButtons(hdc, &pPreviewData->rcInactiveCaption, TRUE, pPreviewData->cyCaption - 2);

    /* Active Window */
    DrawEdge(hdc, &pPreviewData->rcActiveFrame, EDGE_RAISED, BF_RECT | BF_MIDDLE);
    SetTextColor(hdc, pPreviewData->clrSysColor[COLOR_CAPTIONTEXT]);
    DrawCaptionTempW(NULL, hdc, &pPreviewData->rcActiveCaption, pPreviewData->hCaptionFont,
                    NULL, pPreviewData->lpAct, DC_ACTIVE | DC_GRADIENT | DC_ICON | DC_TEXT);
    DrawCaptionButtons(hdc, &pPreviewData->rcActiveCaption, TRUE, pPreviewData->cyCaption - 2);

    /* Draw the menu bar */
    DrawMenuBarTemp(hwnd, hdc, &pPreviewData->rcActiveMenuBar,
                    pPreviewData->hMenu,
                    pPreviewData->hMenuFont);
    /* Draw the client area */
    CopyRect(&rc, &pPreviewData->rcActiveClient);

    DrawEdge(hdc, &rc, EDGE_SUNKEN, BF_RECT | BF_ADJUST);
    FillRect(hdc, &rc, pPreviewData->hbrWindow);

    /* Draw the client text */
    CopyRect(&rc, &pPreviewData->rcActiveClient);
    rc.left += 4;
    rc.top += 2;
    SetTextColor(hdc, pPreviewData->clrSysColor[COLOR_WINDOWTEXT]);

    hOldFont = SelectObject(hdc, pPreviewData->hCaptionFont);
    DrawTextW(hdc, pPreviewData->lpWinTxt, lstrlenW(pPreviewData->lpWinTxt), &rc, DT_LEFT);
    SelectObject(hdc, hOldFont);

    /* Draw the scroll bar */
    DrawScrollbar(hdc, &pPreviewData->rcActiveScroll, pPreviewData->hbrScrollbar);

    /* Dialog Window */
    DrawEdge(hdc, &pPreviewData->rcDialogFrame, EDGE_RAISED, BF_RECT | BF_MIDDLE);
    SetTextColor(hdc, pPreviewData->clrSysColor[COLOR_WINDOW]);
    DrawCaptionTempW(NULL, hdc, &pPreviewData->rcDialogCaption, pPreviewData->hCaptionFont,
                    NULL, pPreviewData->lpMessBox, DC_ACTIVE | DC_GRADIENT | DC_ICON | DC_TEXT);
    DrawCaptionButtons(hdc, &pPreviewData->rcDialogCaption, FALSE, pPreviewData->cyCaption - 2);

    /* Draw the dialog text */
    CopyRect(&rc, &pPreviewData->rcDialogClient);
    rc.left += 4;
    rc.top += 2;
    SetTextColor(hdc, RGB(0,0,0));
    hOldFont = SelectObject(hdc, pPreviewData->hMessageFont);
    DrawTextW(hdc, pPreviewData->lpMessText, lstrlenW(pPreviewData->lpMessText),
        &rc, DT_LEFT);
    SelectObject(hdc, hOldFont);

    /* Draw Button */
    DrawFrameControl(hdc, &pPreviewData->rcDialogButton, DFC_BUTTON, DFCS_BUTTONPUSH);
    CopyRect(&rc, &pPreviewData->rcDialogButton);
    SetTextColor(hdc, pPreviewData->clrSysColor[COLOR_BTNTEXT]);
    hOldFont = SelectObject(hdc, pPreviewData->hMessageFont);
    DrawTextW(hdc, pPreviewData->lpButText, lstrlenW(pPreviewData->lpButText), &rc,
        DT_VCENTER | DT_CENTER | DT_SINGLELINE);
    SelectObject(hdc, hOldFont);

    EndPaint(hwnd, &ps);
}

static VOID OnLButtonDown(HWND hwnd, int xPos, int yPos, PPREVIEW_DATA pPreviewData)
{
    UINT type = COLOR_BACKGROUND;
    POINT pt;

    pt.x = xPos;
    pt.y = yPos;

    if (PtInRect(&pPreviewData->rcInactiveFrame, pt))
        type = COLOR_INACTIVEBORDER;

    if (PtInRect(&pPreviewData->rcInactiveCaption, pt))
        type = COLOR_INACTIVECAPTION;

    if (PtInRect(&pPreviewData->rcActiveFrame, pt))
        type = COLOR_ACTIVEBORDER;

    if (PtInRect(&pPreviewData->rcActiveCaption, pt))
        type = COLOR_ACTIVECAPTION;

    if (PtInRect(&pPreviewData->rcSelectedMenuItem, pt))
        type = COLOR_MENUHILIGHT;

    if (PtInRect(&pPreviewData->rcActiveMenuBar, pt))
        type = COLOR_MENUBAR;

    if (PtInRect(&pPreviewData->rcActiveClient, pt))
        type = COLOR_WINDOW;

    if (PtInRect(&pPreviewData->rcActiveScroll, pt))
        type = COLOR_SCROLLBAR;

    if (PtInRect(&pPreviewData->rcDialogFrame, pt))
        type = -1;

    if (PtInRect(&pPreviewData->rcDialogCaption, pt))
        type = COLOR_ACTIVECAPTION;

    if (PtInRect(&pPreviewData->rcDialogButton, pt))
        type = COLOR_BTNFACE;

    SendMessageW(GetParent(hwnd),
                WM_COMMAND,
                MAKEWPARAM(GetWindowLongPtrW(hwnd, GWLP_ID), BN_CLICKED),
                (LPARAM)type);
}

static VOID OnDestroy(PPREVIEW_DATA pPreviewData)
{
    DeleteObject(pPreviewData->hbrScrollbar);
    DeleteObject(pPreviewData->hbrDesktop);
    DeleteObject(pPreviewData->hbrWindow);

    DeleteObject(pPreviewData->hCaptionFont);
    DeleteObject(pPreviewData->hMenuFont);
    DeleteObject(pPreviewData->hMessageFont);

    DestroyMenu(pPreviewData->hMenu);

    HeapFree(GetProcessHeap(), 0, pPreviewData->lpInAct);
    HeapFree(GetProcessHeap(), 0, pPreviewData->lpAct);
    HeapFree(GetProcessHeap(), 0, pPreviewData->lpWinTxt);
    HeapFree(GetProcessHeap(), 0, pPreviewData->lpMessBox);
    HeapFree(GetProcessHeap(), 0, pPreviewData->lpMessText);
    HeapFree(GetProcessHeap(), 0, pPreviewData->lpButText);
}

static LRESULT CALLBACK PreviewWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    PPREVIEW_DATA pPreviewData;

    pPreviewData = (PPREVIEW_DATA)GetWindowLongPtrW(hwnd, GWLP_USERDATA);

    switch (uMsg)
    {
        case WM_CREATE:
            pPreviewData = (PPREVIEW_DATA)HeapAlloc(GetProcessHeap(),
                                                    HEAP_ZERO_MEMORY,
                                                    sizeof(PREVIEW_DATA));
            if (!pPreviewData)
                return -1;

            SetWindowLongPtrW(hwnd, GWLP_USERDATA, (LONG_PTR)pPreviewData);
            OnCreate(hwnd, pPreviewData);
            break;

        case WM_SIZE:
            OnSize(LOWORD(lParam), HIWORD(lParam), pPreviewData);
            break;

        case WM_PAINT:
            OnPaint(hwnd, pPreviewData);
            break;

        case WM_LBUTTONDOWN:
            OnLButtonDown(hwnd, LOWORD(lParam), HIWORD(lParam), pPreviewData);
            break;

        case WM_DESTROY:
            OnDestroy(pPreviewData);
            HeapFree(GetProcessHeap(), 0, pPreviewData);
            break;

        case PVM_GETCYCAPTION:
            return pPreviewData->cyCaption;

        case PVM_SETCYCAPTION:
            if ((INT)lParam > 0)
            {
                pPreviewData->cyCaption = (INT)lParam;
                CalculateItemSize(pPreviewData);
                InvalidateRect(hwnd, NULL, FALSE);
            }
            break;

        case PVM_GETCYMENU:
            return pPreviewData->cyMenu;

        case PVM_SETCYMENU:
            if ((INT)lParam > 0)
            {
                pPreviewData->cyMenu = (INT)lParam;
                CalculateItemSize(pPreviewData);
                InvalidateRect(hwnd, NULL, FALSE);
            }
            break;

        case PVM_GETCXSCROLLBAR:
            return pPreviewData->cxScrollbar;

        case PVM_SETCXSCROLLBAR:
            if ((INT)lParam > 0)
            {
                pPreviewData->cxScrollbar = (INT)lParam;
                CalculateItemSize(pPreviewData);
                InvalidateRect(hwnd, NULL, FALSE);
            }
            break;

        case PVM_GETCYSIZEFRAME:
            return pPreviewData->cySizeFrame;

        case PVM_SETCYSIZEFRAME:
            if ((INT)lParam > 0)
            {
                pPreviewData->cySizeFrame = (INT)lParam;
                CalculateItemSize(pPreviewData);
                InvalidateRect(hwnd, NULL, FALSE);
            }
            break;

        case PVM_SETCAPTIONFONT:
            CopyMemory(&pPreviewData->lfCaptionFont, (LOGFONTW*)lParam, sizeof(LOGFONTW));
            DeleteObject(pPreviewData->hCaptionFont);
            pPreviewData->hCaptionFont = CreateFontIndirectW(&pPreviewData->lfCaptionFont);
            CalculateItemSize(pPreviewData);
            InvalidateRect(hwnd, NULL, FALSE);
            break;

        case PVM_SETMENUFONT:
            CopyMemory(&pPreviewData->lfMenuFont, (LOGFONTW*)lParam, sizeof(LOGFONTW));
            DeleteObject(pPreviewData->hMenuFont);
            pPreviewData->hMenuFont = CreateFontIndirectW(&pPreviewData->lfMenuFont);
            CalculateItemSize(pPreviewData);
            InvalidateRect(hwnd, NULL, FALSE);
            break;

        case PVM_SETDIALOGFONT:
            CopyMemory(&pPreviewData->lfMessageFont, (LOGFONTW*)lParam, sizeof(LOGFONTW));
            DeleteObject(pPreviewData->hMessageFont);
            pPreviewData->hMessageFont = CreateFontIndirectW(&pPreviewData->lfMessageFont);
            CalculateItemSize(pPreviewData);
            InvalidateRect(hwnd, NULL, FALSE);
            break;

        case PVM_SETCOLOR:
            pPreviewData->clrSysColor[(INT)wParam] = (DWORD)lParam;
            switch((INT)wParam)
            {
                case COLOR_SCROLLBAR:
                    DeleteObject(pPreviewData->hbrScrollbar);
                    pPreviewData->hbrScrollbar =
                        CreateSolidBrush(pPreviewData->clrSysColor[COLOR_SCROLLBAR]);
                    break;

                case COLOR_DESKTOP:
                    DeleteObject(pPreviewData->hbrDesktop);
                    pPreviewData->hbrDesktop =
                        CreateSolidBrush(pPreviewData->clrSysColor[COLOR_DESKTOP]);
                    break;

                case COLOR_WINDOW:
                    DeleteObject(pPreviewData->hbrWindow);
                    pPreviewData->hbrWindow =
                        CreateSolidBrush(pPreviewData->clrSysColor[COLOR_WINDOW]);
                    break;
            }
            InvalidateRect(hwnd, NULL, FALSE);
            break;

        default:
            DefWindowProcW(hwnd, uMsg, wParam, lParam);
    }

    return TRUE;
}

BOOL RegisterPreviewControl(HINSTANCE hInstance)
{
    WNDCLASSEXW wc;

    ZeroMemory(&wc, sizeof(WNDCLASSEXW));

    wc.cbSize = sizeof(WNDCLASSEXW);
    wc.lpfnWndProc = PreviewWndProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursorW(NULL, (LPWSTR) IDC_ARROW);
    wc.hbrBackground = (HBRUSH)NULL;
    wc.lpszClassName = szPreviewWndClass;

    return RegisterClassExW(&wc) != (ATOM)0;
}


VOID UnregisterPreviewControl(HINSTANCE hInstance)
{
    UnregisterClassW(szPreviewWndClass, hInstance);
}

