#ifndef __DESK_CPL_H
#define __DESK_CPL_H

#include "resource.h"

#define MAX_STRING_LEN     1024

/* main.c */
extern HINSTANCE hInstCPL;

/* appearance.c */
INT_PTR CALLBACK AppearanceDlgProc (HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
void set_color_from_theme(WCHAR *keyName, COLORREF color);

/* theme.c */
INT_PTR CALLBACK ThemeDlgProc (HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

#endif
