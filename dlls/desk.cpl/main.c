/*
 * desk.cpl - the Desktop appearance control panel applet
 *
 * Copyright 2002 Jaco Greeff
 * Copyright 2003 Dimitrie O. Paun
 * Copyright 2003 Mike Hearn
 * Copyright 2008 Owen Rudge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define NONAMELESSUNION
#define NONAMELESSSTRUCT

#include "config.h"
#include "wine/port.h"
#include "wine/unicode.h"
#include "wine/debug.h"
#include "wine/list.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <windef.h>
#include <winbase.h>
#include <winuser.h>
#include <winreg.h>
#include <commctrl.h>
#include <cpl.h>

#include "desk.h"
#include "preview.h"

WINE_DEFAULT_DEBUG_CHANNEL(deskcpl);

HINSTANCE hInstCPL;

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason,
                    LPVOID lpvReserved)
{
    TRACE("(%p, %d, %p)\n", hinstDLL, fdwReason, lpvReserved);

    switch (fdwReason) 
    {
        case DLL_PROCESS_ATTACH:
            hInstCPL = hinstDLL;
            break;
    }
    return TRUE;
}

static void doPropertySheet (HINSTANCE hInst, HWND hWnd)
{
    PROPSHEETPAGEW psp[2];
    PROPSHEETHEADERW psh;
    WCHAR tab_title[2][MAX_STRING_LEN], app_title[MAX_STRING_LEN];

    /* Load the strings we will use */
    LoadStringW(hInst, IDS_TAB_THEME, tab_title[0], sizeof(tab_title[0]) / sizeof(tab_title[0][0]));
    LoadStringW(hInst, IDS_TAB_APPEARANCE, tab_title[1], sizeof(tab_title[1]) / sizeof(tab_title[1][0]));
    LoadStringW(hInst, IDS_DESKTOP_TITLE, app_title, sizeof(app_title) / sizeof(app_title[0]));

    /* Fill out the PROPSHEETPAGE */
    psp[0].dwSize = sizeof (PROPSHEETPAGEW);
    psp[0].dwFlags = PSP_USETITLE;
    psp[0].hInstance = hInst;
    psp[0].u.pszTemplate = MAKEINTRESOURCEW (IDD_THEME);
    psp[0].u2.pszIcon = NULL;
    psp[0].pfnDlgProc = (DLGPROC) ThemeDlgProc;
    psp[0].pszTitle = tab_title[0];
    psp[0].lParam = 0;

    psp[1].dwSize = sizeof (PROPSHEETPAGEW);
    psp[1].dwFlags = PSP_USETITLE;
    psp[1].hInstance = hInst;
    psp[1].u.pszTemplate = MAKEINTRESOURCEW (IDD_APPEARANCE);
    psp[1].u2.pszIcon = NULL;
    psp[1].pfnDlgProc = (DLGPROC) AppearanceDlgProc;
    psp[1].pszTitle = tab_title[1];
    psp[1].lParam = 0;

    /* Fill out the PROPSHEETHEADER */
    psh.dwSize = sizeof (PROPSHEETHEADERW);
    psh.dwFlags = PSH_PROPSHEETPAGE | PSH_USEICONID;
    psh.hwndParent = hWnd;
    psh.hInstance = hInst;
    psh.u.pszIcon = NULL;
    psh.pszCaption = app_title;
    psh.nPages = 2;
    psh.u3.ppsp = psp;
    psh.pfnCallback = NULL;
    psh.u2.nStartPage = 0;

    /* Display the property sheet */
    PropertySheetW (&psh);
}

LONG CALLBACK CPlApplet(HWND hwndCPL,
                        UINT message, LPARAM lParam1, LPARAM lParam2)
{
    CPLINFO *appletInfo = (CPLINFO *) lParam2;

    switch (message) 
    {
        case CPL_INIT:
            InitCommonControls();
            RegisterPreviewControl(hInstCPL);
            return TRUE;

        case CPL_GETCOUNT:
            return 1;

        case CPL_INQUIRE:
            appletInfo->idIcon = ICO_DESKTOP_ICON;
            appletInfo->idName = IDS_DESKTOP_TITLE;
            appletInfo->idInfo = IDS_DESKTOP_DESCRIPTION;
            appletInfo->lData = 0;

            break;

        case CPL_DBLCLK:
            doPropertySheet(hInstCPL, hwndCPL);
            break;

        case CPL_EXIT:
            UnregisterPreviewControl(hInstCPL);
            break;
    }

    return FALSE;
}

