/*
 * Desktop control panel
 * Preview window
 *
 * Copyright (c) 2006, 2007 Eric Kohl
 * Copyright (c) 2008 Owen Rudge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __PREVIEW_H
#define __PREVIEW_H

#define PVM_GETCYCAPTION     (WM_USER+1)
#define PVM_SETCYCAPTION     (WM_USER+2)

#define PVM_GETCYMENU        (WM_USER+3)
#define PVM_SETCYMENU        (WM_USER+4)

#define PVM_GETCXSCROLLBAR   (WM_USER+5)
#define PVM_SETCXSCROLLBAR   (WM_USER+6)

#define PVM_GETCYSIZEFRAME   (WM_USER+7)
#define PVM_SETCYSIZEFRAME   (WM_USER+8)

#define PVM_SETCAPTIONFONT   (WM_USER+9)
#define PVM_SETMENUFONT      (WM_USER+10)
#define PVM_SETDIALOGFONT    (WM_USER+11)

#define PVM_SETCOLOR         (WM_USER+12)

BOOL RegisterPreviewControl(HINSTANCE hInstance);
VOID UnregisterPreviewControl(HINSTANCE hInstance);

#endif
