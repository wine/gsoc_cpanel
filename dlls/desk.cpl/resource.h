#ifndef __DESK_RSRC_H
#define __DESK_RSRC_H

#define ICO_DESKTOP_ICON                       101

#define IDS_DESKTOP_TITLE                       11
#define IDS_DESKTOP_DESCRIPTION                 12

#define IDS_TAB_APPEARANCE                      20
#define IDS_TAB_THEME                           21

#define IDS_NOTHEME                             22

#define IDS_THEMEFILE                           23
#define IDS_THEMEFILE_SELECT                    24

#define IDD_APPEARANCE                          10
#define IDD_THEME                               11

/* Appearance tab */
#define IDC_STATIC                              -1

#define IDC_THEME_COLORCOMBO                  1401
#define IDC_THEME_COLORTEXT                   1402
#define IDC_THEME_SIZECOMBO                   1403
#define IDC_THEME_SIZETEXT                    1404
#define IDC_THEME_THEMECOMBO                  1405
#define IDC_THEME_INSTALL                     1406

#define IDC_SYSPARAM_COMBO                    1411
#define IDC_SYSPARAM_SIZE_TEXT                1412
#define IDC_SYSPARAM_SIZE                     1413
#define IDC_SYSPARAM_SIZE_UD                  1414
#define IDC_SYSPARAM_COLOR_TEXT               1415
#define IDC_SYSPARAM_COLOR                    1416
#define IDC_SYSPARAM_FONT                     1417
#define IDC_APPEARANCE_PREVIEW                1418

#define IDC_SYSPARAMS_BUTTON                  1500
#define IDC_SYSPARAMS_BUTTON_TEXT             1501
#define IDC_SYSPARAMS_DESKTOP                 1502
#define IDC_SYSPARAMS_MENU                    1503
#define IDC_SYSPARAMS_MENU_TEXT               1504
#define IDC_SYSPARAMS_SCROLLBAR               1505
#define IDC_SYSPARAMS_SELECTION               1506
#define IDC_SYSPARAMS_SELECTION_TEXT          1507
#define IDC_SYSPARAMS_TOOLTIP                 1508
#define IDC_SYSPARAMS_TOOLTIP_TEXT            1509
#define IDC_SYSPARAMS_WINDOW                  1510
#define IDC_SYSPARAMS_WINDOW_TEXT             1511
#define IDC_SYSPARAMS_ACTIVE_TITLE            1512
#define IDC_SYSPARAMS_ACTIVE_TITLE_TEXT       1513
#define IDC_SYSPARAMS_INACTIVE_TITLE          1514
#define IDC_SYSPARAMS_INACTIVE_TITLE_TEXT     1515
#define IDC_SYSPARAMS_MSGBOX_TEXT             1516
#define IDC_SYSPARAMS_APPWORKSPACE            1517
#define IDC_SYSPARAMS_WINDOW_FRAME            1518
#define IDC_SYSPARAMS_ACTIVE_BORDER           1519
#define IDC_SYSPARAMS_INACTIVE_BORDER         1520
#define IDC_SYSPARAMS_BUTTON_SHADOW           1521
#define IDC_SYSPARAMS_GRAY_TEXT               1522
#define IDC_SYSPARAMS_BUTTON_HILIGHT          1523
#define IDC_SYSPARAMS_BUTTON_DARK_SHADOW      1524
#define IDC_SYSPARAMS_BUTTON_LIGHT            1525
#define IDC_SYSPARAMS_BUTTON_ALTERNATE        1526
#define IDC_SYSPARAMS_HOT_TRACKING            1527
#define IDC_SYSPARAMS_ACTIVE_TITLE_GRADIENT   1528
#define IDC_SYSPARAMS_INACTIVE_TITLE_GRADIENT 1529
#define IDC_SYSPARAMS_MENU_HILIGHT            1530
#define IDC_SYSPARAMS_MENUBAR                 1531

/* Preview window */
#define IDR_PREVIEW_MENU                      1600
#define ID_MENU_NORMAL                        1601
#define ID_MENU_DISABLED                      1602
#define ID_MENU_SELECTED                      1603

#define IDS_INACTWIN                          1700
#define IDS_ACTWIN                            1701
#define IDS_WINTEXT                           1702
#define IDS_MESSBOX                           1703
#define IDS_MESSTEXT                          1704
#define IDS_BUTTEXT                           1705

#endif
