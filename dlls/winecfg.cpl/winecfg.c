/*
 * Winecfg.cpl - the control panel applet for wine's utilites
 *
 * Copyright 2008 Metody Stefanov
 * Copyright 2008 Owen Rudge
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */
#include "config.h"
#include "wine/port.h"
#include "wine/unicode.h"
#include "wine/debug.h"

#include <stdarg.h>
#include <windef.h>
#include <winbase.h>
#include <winuser.h>
#include <shellapi.h>
#include <cpl.h>

#include "winecfg.h"

WINE_DEFAULT_DEBUG_CHANNEL(winecfgcpl);

void StartApplet(int NumOfApplet);

#define NUM_APPLETS   4

CPLINFO WinecfgCPLApplets[NUM_APPLETS] =
{
    {ICO_WINECFG_ICON, IDS_WINECFG_TITLE, IDS_WINECFG_DESCRIPTION},
    {ICO_REGEDIT_ICON, IDS_REGEDIT_TITLE, IDS_REGEDIT_DESCRIPTION},
    {ICO_TASKMGR_ICON, IDS_TASKMGR_TITLE, IDS_TASKMGR_DESCRIPTION},
    {ICO_DESKTOP_ICON, IDS_DESKTOP_TITLE, IDS_DESKTOP_DESCRIPTION}
};

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason,
                    LPVOID lpvReserved)
{
    TRACE("(%p, %d, %p)\n", hinstDLL, fdwReason, lpvReserved);

    switch (fdwReason) 
    {
        case DLL_THREAD_ATTACH:
        case DLL_PROCESS_ATTACH:
            break;
    }
    return TRUE;
}

LONG CALLBACK CPlApplet(HWND hwndCPL,
                        UINT message, LPARAM lParam1, LPARAM lParam2)
{
    int CPLAppNumber = (int)lParam1; 

    switch (message) 
    {
        case CPL_INIT:
        {
            return TRUE;
        }

        case CPL_GETCOUNT:
        {
            return NUM_APPLETS;
        }

        case CPL_INQUIRE:
        {
            CPLINFO *appletInfo = (CPLINFO *) lParam2;

            appletInfo->idIcon = WinecfgCPLApplets[CPLAppNumber].idIcon;
            appletInfo->idName = WinecfgCPLApplets[CPLAppNumber].idName;
            appletInfo->idInfo = WinecfgCPLApplets[CPLAppNumber].idInfo;
            appletInfo->lData = 0;

            break;
        }

        case CPL_DBLCLK:
        {
            StartApplet(CPLAppNumber);
            break;
        }
    }

    return FALSE;
}

void StartApplet(int NumOfApplet)
{

    switch (NumOfApplet)
    {
        case 0:
        {
            ShellExecuteA(NULL, "open", "winecfg", NULL, NULL, SW_SHOWNORMAL);
            break;
        }

        case 1:
        {
            ShellExecuteA(NULL, "open", "regedit", NULL, NULL, SW_SHOWNORMAL);
            break;
        }

        case 2:
        {
            ShellExecuteA(NULL, "open", "taskmgr", NULL, NULL, SW_SHOWNORMAL);
            break;
        }

        case 3:
        {
            ShellExecuteA(NULL, "open", "winecfg", "/title:desktop desk", NULL, SW_SHOWNORMAL);
            break;
        }
    }
}
